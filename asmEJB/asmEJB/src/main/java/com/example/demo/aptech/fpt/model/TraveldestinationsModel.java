package com.example.demo.aptech.fpt.model;

import com.example.demo.aptech.fpt.entiy.Tourguide;
import com.example.demo.aptech.fpt.entiy.Traveldestinations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TraveldestinationsModel extends JpaRepository<Traveldestinations,Integer> {

    @Query("SELECT p FROM Traveldestinations p WHERE p.name like %:name%")
    List<Traveldestinations> searchByName(@Param("name") String name );
}



