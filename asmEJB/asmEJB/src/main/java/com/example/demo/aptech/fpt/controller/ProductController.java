//package com.example.demo.aptech.fpt.controller;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.*;
//
//@Controller
//public class ProductController {
//    @Autowired
//    ProductModel productModel;
//
//
//
//    @RequestMapping(path = "/index", method = RequestMethod.GET)
//    public String index(Model model)
//    {
//
//        model.addAttribute( "list",productModel.findAllBy());
//        return "index";
//    }
//    @RequestMapping(path = "/product", method = RequestMethod.GET)
//    public String createProduct(@ModelAttribute Product product)
//    {
//        return "product-form";
//    }
//
//    @RequestMapping(path = "/product", method = RequestMethod.POST)
//    public String saveProduct(Product product, Model model)
//    {
//        productModel.save(product);
//        model.addAttribute( "product",product );
//
//        model.addAttribute( "list",productModel.findAllBy());
//        return "index";
//
//    }
//
//
//    @RequestMapping(path = "/delete",  method = RequestMethod.GET)
//    public String delete(@RequestParam("id") int id,Model model)
//    {
//
//        productModel.deleteById(id);
//        model.addAttribute( "list",productModel.findAllBy());
//        return "index";
//    }
//
//    @RequestMapping(path = "/update",  method = RequestMethod.GET)
//    public String update(@RequestParam("id") int id,Model model)
//    {
//        model.addAttribute( "product",productModel.findID(id));
//        return "PersonalProfile";
//    }
//
//    @RequestMapping(path = "/update", method = RequestMethod.POST)
//    public String saveupdate(Product product, Model model)
//    {
//        productModel.save(product );
//        model.addAttribute( "list",productModel.findAllBy());
//        return "index";
//
//    }
//
//
//    @RequestMapping(path = "/search",  method = RequestMethod.GET)
//    public String search(Product product)
//    {
//
//        return "search";
//    }
//
//    @RequestMapping(path = "/search", method = RequestMethod.POST)
//    public String searchview(Product product, Model model)
//    {
//
//        model.addAttribute( "list",productModel.searchByName(product.getName()));
//        return "index";
//
//    }
//
//}
