package com.example.demo.aptech.fpt.controller;


import com.example.demo.aptech.fpt.entiy.Tourguide;
import com.example.demo.aptech.fpt.model.AccountModel;
import com.example.demo.aptech.fpt.model.TourGuideModel;
import com.example.demo.aptech.fpt.model.TraveldestinationsModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;


@Controller
public class AdminController {
    @Autowired
    AccountModel accountModel;
    @Autowired
    TourGuideModel tourGuideModel;
    @Autowired
    TraveldestinationsModel traveldestinationsModel;




    @RequestMapping(path = "/admin/tourguide", method = RequestMethod.GET)
    public String getAllProducts(Model model, @RequestParam(defaultValue = "-1") int page, HttpSession session) {
        int p = 0;
        Tourguide tourguide = new Tourguide();
        if(page ==-1){

            if(session.getAttribute("page")!=null){
                String a = session.getAttribute("page").toString();

                p = Integer.parseInt(a);
            }else {
                session.setAttribute("page", 0);
                String a = session.getAttribute("page").toString();
                p = Integer.parseInt(a);
            }
        }else {
            p=page;
            session.setAttribute("page", p);
        }
        model.addAttribute("tourguide", tourguide);
        model.addAttribute("listtg", tourGuideModel.findAll(new PageRequest(p,8)));
        model.addAttribute("currentPage",p);
        return "admin/tourguide";
    }
    @RequestMapping(path = "/edit", method = RequestMethod.POST)
    public String saveProduct(Tourguide tourguide) {
        System.out.println(tourguide.getFullname());
        tourGuideModel.save(tourguide);
        return "redirect:/admin/tourguide";
    }

    @RequestMapping(path = "/admin/tourguide/delete/{id}", method = RequestMethod.GET)
    public String deleteProduct(@PathVariable(name = "id") int id) {
        tourGuideModel.deleteById(id);
        return "redirect:/admin/tourguide";
    }

}
