package com.example.demo.aptech.fpt.controller;

import com.example.demo.aptech.fpt.entiy.Account;
import com.example.demo.aptech.fpt.entiy.Search;
import com.example.demo.aptech.fpt.entiy.Tourguide;
import com.example.demo.aptech.fpt.entiy.Traveldestinations;
import com.example.demo.aptech.fpt.model.AccountModel;
import com.example.demo.aptech.fpt.model.TourGuideModel;
import com.example.demo.aptech.fpt.model.TraveldestinationsModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Controller
public class AuthController {
    @Autowired
    AccountModel accountModel;
    @Autowired
    TourGuideModel tourGuideModel;
    @Autowired
    TraveldestinationsModel traveldestinationsModel;
//    @Autowired
//    ProductModel productModel;



    @PostMapping("/api/search")
    public ResponseEntity<?> getSearchResultViaAjax( HttpServletRequest request,HttpServletResponse response) {
        String input = request.getParameter("search");
        List<Tourguide> sc=new LinkedList<>();
      sc=tourGuideModel.searchByName(input);

        List<Traveldestinations> travel=new LinkedList<>();
        travel=traveldestinationsModel.searchByName(input);

        List<Search> s=new LinkedList<>();
        for (Tourguide tourguide :sc) {
            Search search=new Search();
            search.setLink("/PersonalProfile?id="+tourguide.getId());
            search.setName(tourguide.getFullname()+" ("+tourguide.getCardnumber()+")");
       s.add(search);
        }

        for (Traveldestinations traveldestinations :travel) {
            Search search=new Search();
            search.setLink("/"+traveldestinations.getId());
            search.setName(traveldestinations.getName()+"("+traveldestinations.getAddress()+")");
            s.add(search);
        }
        return ResponseEntity.ok(s);

    }



    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String uploadMultiFileHandlerPOST(HttpServletRequest request, //
                                             Model model) {

        String input = request.getParameter("input");
        List<Traveldestinations> travel=new LinkedList<>();
        travel=traveldestinationsModel.searchByName(input);
        System.out.println(input);
        Traveldestinations traveldestinations =new Traveldestinations();
        model.addAttribute("traveldestinations",traveldestinations);
        model.addAttribute("listtv",travel);
        return "search";

    }
    @RequestMapping(path = "/sr", method = RequestMethod.GET)
    public String pp(@ModelAttribute Account account, HttpServletRequest request, HttpServletResponse response) throws IOException {


        return "test/menu";
    }

    public Map<Integer, String> getSearch(String id) {


            Map<Integer, String> list = new HashMap<Integer, String>();


    list.put(0, id);


            return list;


    }

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public String checkLogin(Account account,Model model)
    {
        Account ac=new Account();
      ac=   accountModel.checkLogin(account.getUsername(),account.getPassword());
      try {
          if (!ac.getUsername().isEmpty()) {


          //    model.addAttribute( "list",productModel.findAllBy());
              return "index";
          } else {
              return "login";
          }
      }catch (Exception ex){
          model.addAttribute( "messenger","Login eror");
          return "login";
      }
    }


    @RequestMapping(path = "/PersonalProfile",  method = RequestMethod.GET)
    public String update(@RequestParam("id") int id,Model model)
    {
        model.addAttribute( "tourGuide",tourGuideModel.findID(id));
        return "PersonalProfile";
    }
}
