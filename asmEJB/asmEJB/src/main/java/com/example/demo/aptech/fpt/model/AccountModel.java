package com.example.demo.aptech.fpt.model;

import com.example.demo.aptech.fpt.entiy.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AccountModel extends JpaRepository<Account,Integer> {



    @Query("SELECT p FROM Account p WHERE LOWER(p.username) = LOWER(:username) AND LOWER(p.password)=LOWER(:password) ")
    Account checkLogin(@Param("username") String username,@Param("password") String password);

}



