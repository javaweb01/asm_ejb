package com.example.demo.aptech.fpt.model;

import com.example.demo.aptech.fpt.entiy.Account;


import com.example.demo.aptech.fpt.entiy.Tourguide;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TourGuideModel extends JpaRepository<Tourguide,Integer> {

    @Query("SELECT p FROM Tourguide p WHERE LOWER(p.id) = LOWER(:id)")
    Tourguide findID(@Param("id") int id);


    @Query("SELECT p FROM Tourguide p WHERE p.fullname like %:fullname%")
    List<Tourguide> searchByName(@Param("fullname") String fullname );



}



