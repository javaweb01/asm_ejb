package com.example.demo.aptech.fpt.entiy;

public class Search {
    private String link;
    private String name;

    public Search(String link, String name) {
        this.link = link;
        this.name = name;
    }

    public Search() {

    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
