package com.example.demo.aptech.fpt.entiy;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity

public class Tourguide {
    private MultipartFile[] fileDatas;

    public MultipartFile[] getFileDatas() {
        return fileDatas;
    }

    public void setFileDatas(MultipartFile[] fileDatas) {
        this.fileDatas = fileDatas;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String avatar;
    private String fullname;
    private String birthday;
    private String email;
    private String photocard;
    private String phonenumber;
    private String cardnumber;
    private String expirydate;
    private String status;
    private String cardtype;
    private String placeofissue;
    private String foreignlanguage;

    public Tourguide() {
    }

    public Tourguide(String avatar, String fullname, String birthday, String email, String photocard, String phonenumber, String cardnumber, String expirydate, String status, String cardtype, String placeofissue, String foreignlanguage) {
        this.avatar = avatar;
        this.fullname = fullname;
        this.birthday = birthday;
        this.email = email;
        this.photocard = photocard;
        this.phonenumber = phonenumber;
        this.cardnumber = cardnumber;
        this.expirydate = expirydate;
        this.status = status;
        this.cardtype = cardtype;
        this.placeofissue = placeofissue;
        this.foreignlanguage = foreignlanguage;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhotocard() {
        return photocard;
    }

    public void setPhotocard(String photocard) {
        this.photocard = photocard;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCardtype() {
        return cardtype;
    }

    public void setCardtype(String cardtype) {
        this.cardtype = cardtype;
    }

    public String getPlaceofissue() {
        return placeofissue;
    }

    public void setPlaceofissue(String placeofissue) {
        this.placeofissue = placeofissue;
    }

    public String getForeignlanguage() {
        return foreignlanguage;
    }

    public void setForeignlanguage(String foreignlanguage) {
        this.foreignlanguage = foreignlanguage;
    }


}
