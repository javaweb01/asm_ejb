package com.example.demo.aptech.fpt.controller;

import com.example.demo.aptech.fpt.entiy.Tourguide;
import com.example.demo.aptech.fpt.entiy.Traveldestinations;
import com.example.demo.aptech.fpt.model.TourGuideModel;
import com.example.demo.aptech.fpt.model.TraveldestinationsModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MyFileUploadController {
    @Autowired
    TourGuideModel tourGuideModel;
    @Autowired
    TraveldestinationsModel traveldestinationsModel;
    @RequestMapping(value = "/")
    public String homePage() {

        return "_index";
    }


    @RequestMapping(value = "/uploadTravel", method = RequestMethod.GET)
    public String uploadTravel(Model model) {

        Traveldestinations traveldestinations = new Traveldestinations();

        model.addAttribute("myUploadForm", traveldestinations);

        return "Addtraveldestinations ";
    }


    @RequestMapping(value = "/uploadTravel", method = RequestMethod.POST)
    public String uploadTravelPOST(HttpServletRequest request, //
                                   Model model, //
                                   @ModelAttribute("myUploadForm") Traveldestinations traveldestinations) {

        return this.doUploadTravel(request, model, traveldestinations);

    }


    @RequestMapping(value = "/uploadMultiFile", method = RequestMethod.GET)
    public String uploadMultiFileHandler(Model model) {

        Tourguide tourGuideEntity = new Tourguide();
        model.addAttribute("myUploadForm", tourGuideEntity);

        return "_uploadMultiFile";
    }


    @RequestMapping(value = "/uploadMultiFile", method = RequestMethod.POST)
    public String uploadMultiFileHandlerPOST(HttpServletRequest request, //
                                             Model model, //
                                             @ModelAttribute("myUploadForm") Tourguide tourGuideEntity) {

        return this.doUpload(request, model, tourGuideEntity);

    }


    private String doUploadTravel(HttpServletRequest request, Model model, //
                                  Traveldestinations traveldestinations) {
String img=null;
        String uploadRootPath = "C:\\Users\\Admin\\Desktop\\asmEJB\\asmEJB\\src\\main\\resources\\static\\images";
        int count = 0;
        Traveldestinations traveldestinations1 = new Traveldestinations();
        File uploadRootDir = new File(uploadRootPath);
        // Tạo thư mục gốc upload nếu nó không tồn tại.
        if (!uploadRootDir.exists()) {
            uploadRootDir.mkdirs();
        }
        MultipartFile[] fileDatas = traveldestinations.getFileDatas();
        //
        List<File> uploadedFiles = new ArrayList<File>();
        List<String> failedFiles = new ArrayList<String>();
        for (MultipartFile fileData : fileDatas) {
            // Tên file gốc tại Client.
            String name = fileData.getOriginalFilename();
            if (name != null && name.length() > 0) {
                try {
                    // Tạo file tại Server.
                    File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                    stream.write(fileData.getBytes());
                    stream.close();
                    //
                    uploadedFiles.add(serverFile);
                    System.out.println("Write file: " + serverFile.getName());
                   img=serverFile.getName();
                    count++;
                } catch (Exception e) {
                    System.out.println("Error Write file: " + name);
                    failedFiles.add(name);
                }
            }
        }

        traveldestinations1.setName(traveldestinations.getName());
        traveldestinations1.setImage("/images/"+img);
        traveldestinations1.setAddress(traveldestinations.getAddress());
        traveldestinations1.setDescription(traveldestinations.getDescription());
        traveldestinationsModel.save(traveldestinations1);
        model.addAttribute("uploadedFiles", uploadedFiles);
        model.addAttribute("failedFiles", failedFiles);
        return "_menu";
    }



    private String doUpload(HttpServletRequest request, Model model,
                            Tourguide tourGuideEntity) {

        String uploadRootPath = "C:\\Users\\Admin\\Desktop\\asmEJB\\asmEJB\\src\\main\\resources\\static\\images";
    int count=0;
        Tourguide tourGuide =new Tourguide();
        File uploadRootDir = new File(uploadRootPath);
        // Tạo thư mục gốc upload nếu nó không tồn tại.
        if (!uploadRootDir.exists()) {
            uploadRootDir.mkdirs();
        }
        MultipartFile[] fileDatas = tourGuideEntity.getFileDatas();
        //
        List<File> uploadedFiles = new ArrayList<File>();
        List<String> failedFiles = new ArrayList<String>();
        for (MultipartFile fileData : fileDatas) {
            // Tên file gốc tại Client.
            String name = fileData.getOriginalFilename();
            if (name != null && name.length() > 0) {
                try {
                    // Tạo file tại Server.
                    File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                    stream.write(fileData.getBytes());
                    stream.close();
                    //
                    uploadedFiles.add(serverFile);
                    System.out.println("Write file: " + serverFile.getName());
if(count==0){
    tourGuide.setAvatar(serverFile.getName());
}else {
    tourGuide.setPhotocard(serverFile.getName());
}
count++;
                } catch (Exception e) {
                    System.out.println("Error Write file: " + name);
                    failedFiles.add(name);
                }
            }
        }
        tourGuide.setBirthday(tourGuideEntity.getBirthday());
        tourGuide.setCardnumber(tourGuideEntity.getCardnumber());
        tourGuide.setPhonenumber(tourGuideEntity.getPhonenumber());
        tourGuide.setCardtype(tourGuideEntity.getCardtype());
        tourGuide.setEmail(tourGuideEntity.getEmail());
        tourGuide.setExpirydate(tourGuideEntity.getExpirydate());
        tourGuide.setFullname(tourGuideEntity.getFullname());
        tourGuide.setPlaceofissue(tourGuide.getPlaceofissue());
        tourGuide.setForeignlanguage(tourGuideEntity.getForeignlanguage());
        tourGuide.setStatus("N");
        tourGuideModel.save(tourGuide);
        model.addAttribute("uploadedFiles", uploadedFiles);
        model.addAttribute("failedFiles", failedFiles);
        return "_uploadResult";
    }

}