create database travel;


create table account(
username nvarchar(255)PRIMARY KEY  not null,
password nvarchar(255),
)

create table tourguide(
id int PRIMARY KEY  not null,
avatar nvarchar(255),
photocard nvarchar(255),
fullname nvarchar(255),
birthday nvarchar(255),
email nvarchar(255),
phonenumber nvarchar(255),
cardnumber nvarchar(255),
expirydate  nvarchar(255),
status nvarchar(255),
cardtype nvarchar(255),
placeofissue nvarchar(255),
foreignlanguage nvarchar(255),
)

create table traveldestinations(
id int PRIMARY KEY  not null,
name nvarchar(255),
image nvarchar(255),
address  nvarchar(255),
description  nvarchar(255)
);


